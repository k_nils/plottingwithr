---
title: "Visual Exploration and Handling of Biological Data in R"
author: "Stefano Secchia and Nils Kurzawa adapted from Bernd Klaus"
date: "BTM 2018"
output: 
  slidy_presentation:
    df_print: tibble
    highlight: kate
    font_adjustment: +1
    fig_width: 6
    fig_height: 3.71
    #css: data_handling.css
---

```{r setup, include=FALSE}
library("knitr")
library("tidyverse")
library("stringr")
library("hexbin")

options(digits = 3, width = 80)
opts_chunk$set(echo = TRUE, tidy = FALSE, include = TRUE,
               dev = 'png', fig.width = 6, fig.asp = 0.618, 
               comment = '  ', dpi = 300,
cache = TRUE)
```


## Graphics in R

* Base R graphics: simple, procedural, canvas-oriented

* High-level approach: in the _grammar of graphics_, **ggplot2** 

* graphics are built up from modular logical pieces

*  we can easily try different visualization types for our data in an easy way


## Base R graphics example


* The most basic function is `plot`, `qplot` is similar

* The data plotted  is from an (ELISA) assay

* assay was used to quantify the activity of the enzyme deoxyribonuclease (DNase)

* The data are in the R object `DNase`, which conveniently comes with base R.

```{r basicplotting1, fig.show='hide'}
head(DNase)
```


----


```{r basicplotting2, fig.show='show', fig.height=3, fig.width=5.5}
plot(DNase$conc, DNase$density)
```



## ggplot2 plotting

* base R plotting functions are great for quick interactive exploration

* ...it is limited, ggplot2 enables step by step construction of high quality
graphics 



## A more complex data set {#complex }

<img src="Yusukecells-2.jpg" width="35%" height="35%" />

* Gene expression microarray dataset from 100 individual cells from mouse embryos at different time points

* The Image shows a  single--section immunofluorescence image of the E3.5 mouse blastocyst

* stained for Serpinh1, a marker of primitive endoderm (blue), Gata6 (red) and Nanog (green)


## We take a look at the sample annotation for the  data

Let's have a look at what information is available about the samples

```{r xpData, message=FALSE,warning=FALSE}
library("Hiiragi2013")
data("x")
sample_n(pData(x), 5)
```

* __pData__ means phenotypic data 

## Summarizing the info about the experimental groups

* with  **group\_by** and summarize functions from  **dplyr**,
we get a data frame __groups__ that contains
summary information for each group: the number of cells and the preferred color.


```{r groupSize}
groups <- group_by(pData(x), sampleGroup) %>%
  dplyr::summarize(n = n() , color = unique(sampleColour))
groups
```

* FGF4 is an important regulator of cell differentiation

* Starting from E3.5 the WT cells differentiate into different cell lineages

* pluripotent epiblast (EPI) and primitive endoderm (PE)

## Plotting the DNase data with ggplot2

* __ggplot2__ implements the concept of the grammar of graphics

* extensive docs: <http://docs.ggplot2.org>

```{r figredobasicplottingwithggplot, fig.width = 3.75, fig.height = 3.75}
ggplot(DNase, aes(x = conc, y = density)) + geom_point()
```

## Disecting the ggplot2 command


```{r figredobasicplottingwithggplot_2, eval = FALSE, fig.width = 3.75, fig.height = 3.75}
ggplot(DNase, aes(x = conc, y = density)) + geom_point()
```


* ggplot2 works by mapping __data to aesthetics__ (things you can visually perceive)

* ... and then adding __layers__ to the plot that govern certain aspects

* no canvas model, you build the plot layer by layer

## A bar plot ...

*  ... of the number of samples for each of the  groups  for microarray data

```{r figqplot2, fig.width = 3.75, fig.height = 3.75}
groupColor <- setNames(groups$color, groups$sampleGroup)

ggplot(groups, aes(x = sampleGroup, y = n, fill = sampleGroup)) +
  geom_bar(stat = "identity") +
  scale_fill_manual(values = groupColor, name = "Groups") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1))
```


## The grammar of graphics

The components of __ggplot2__'s grammar of graphics are

1. one or more datasets.

2. one or more geometric objects that serve as the visual representations of the data
  -- for instance, points, lines, rectangles, contours

3. descriptions of how the variables in the data are mapped to visual properties
  (aesthetics) of the geometric objects

4. one or more coordinate systems

5. statistical summarization rules

6. a facet specification, i.e. the use of multiple similar subplots to look at 
  subsets of the same data

7. optional parameters that affect the layout and rendering, such text size, font and
  alignment, legend positions, and the like
  

## A scatterplot of gene expression data

```{r figscp2layers1, fig.width = 3.75, fig.height = 3.75}
dftx <- as_tibble(t(exprs(x))) %>% cbind(pData(x))
#dftx$sampleGroup <- factor(dftx$sampleGroup, levels = unique(dftx$sampleGroup))

ggplot( dftx, aes( x = `1426642_at`, y = `1418765_at` ))  +
  geom_point( aes(color = sampleGroup), shape = 19 ) +
  geom_smooth( method = "loess" ) +
  scale_color_manual(values = groupColor ) +
  xlab("Fn1") +
  ylab("Timd2") +
  coord_equal()
```

* Timd2 is consistently high in the early time points

* its expression goes down in the EPI samples at days 3.5 and 4.5

* In the FGF4-KO, this decrease is delayed  

* Conversely, the gene Fn1 goes up at days 3.5 and 4.5

## Visualization of 1D data

* we'll use a set of four genes: Fgf4, Gata4, Gata6 and Sox2 

* we use the function __gather__ from the __tidyr__ package

```{r genes2ps1}
selectedProbes <- c( Fgf4 = "1420085_at", Gata4 = "1418863_at",
                   Gata6 = "1425463_at",  Sox2 = "1416967_at")

genes <- rownames_to_column(as.data.frame(exprs(x)[selectedProbes, ]), 
                            var = "probe")
genes <- gather(genes, 
               key = sample, 
               value = value, -probe)

genes$gene <- names(selectedProbes)[ match(genes$probe, selectedProbes) ]
head(genes)
```


## A Barplot --- don't do it like this!


```{r onedbp2, fig.width = 3.75, fig.height = 3.75}
ggplot(genes, aes( x = gene, y = value, fill = gene)) +
  stat_summary(fun.y = mean, geom = "bar") +
  stat_summary(fun.data = mean_cl_normal, geom = "errorbar",
               width = 0.25)
```

* common but bad for highly skewed distributions or datasets with outliers

## Boxplots


```{r onedboxpl, fig.width = 3.75, fig.height = 3.75}
p <- ggplot(genes, aes( x = gene, y = value, fill = gene))
p + geom_boxplot()
```


* this takes a similar amount of space, but is much more
informative

* two of the genes (Gata4, Gata6) have relatively concentrated distributions

* for Fgf4 the distribution is right-skewed

## Dot plots and beeswarm plots

* show the data points directly if not too many 

* overplotting avoided by clever arrangement

```{r oneddot, fig.width = 3.75, fig.height = 3.75}
p + geom_dotplot(binaxis = "y", binwidth = 1/6,
       stackdir = "center", stackratio = 0.75,
       aes(color = gene))
```


## Density plots

Yet another way to represent the same data is by density plots

```{r oneddens, fig.width = 3.75, fig.height = 3.75}
ggplot(genes, aes( x = value, color = gene)) + geom_density()
```

Density estimation has a number of complications:

* smoothing windows size is difficult

* shows sample size

## ECDF plots

* cumulative distribution function (CDF) :
$$
F(x) = P(X\le x),
$$


* empirical cumulative distribution function (ECDF) :

$$
F_{n}(x) = \frac{1}{n}\sum_{i=1}^n \mathbf{1}_{x_i\le x}.
$$

```{r onedecdf, fig.width = 3.75, fig.height = 3.75}
ggplot(genes, aes( x = value, color = gene)) + stat_ecdf()
```


The ECDF has several nice properties:

  * It is lossless --- the ECDF \(F_{n}(x)\) contains all the information contained in the original sample  \(x_1,\ldots,x_n\) 
   
 * As \(n\) grows, the ECDF \(F_{n}(x)\) converges to the true CDF \(F(x)\).

## Visualization of 2D data: scatterplots

*  useful for visualizing treatment--response comparisons, associations between variables etc.

* we look at differential expression between a wildtype and an FGF4-KO sample

```{r twodsp1, fig.width = 3.75, fig.height = 3.75}
dfx <- as.data.frame(exprs(x))
scp <- ggplot(dfx, aes(x = `59 E4.5 (PE)` ,
                      y = `92 E4.5 (FGF4-KO)`))
scp + geom_point(alpha = 0.1)
```



* The slight transparency alleviates overplotting





